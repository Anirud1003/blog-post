# Error handling with promises and why is it important.

Before we go into error handling in promises let us understand what promises are in javascript.

A `promise` is a javascript object that allows us to make async calls. It will return a value when the async operation is completed successfully or will throw an error.

## Creating a promise
```
let promise = new promise(function(resolve,reject)) {
    fs.readfile("somePath","utf-8", (error,data)=>{
        if(error){
            reject(error)
        } else {
            resolve(data)
        }
    })
}

```
## Different states of promises

There are 3 states in promises
- pending : The promise is still working and is waiting for something to happen
- fullfilled : After successfull completion of task it will return a value
- rejected : The promise fails and will throw an error

## Error handling importance

Error handling places a crucial part in programming because if we doesn't know how to handle it may lead to unexpected behaviour of the our code.And that to its gonna play a very crucial role handling error with promises because promises are objects that represent the result of an asynchronous opertaion.  

## Ways to handle errors in promises

- using `.catch` method

The catch method allows us to handle errors in a promise chain. Even if a single then block fails, the catch method gets called and error will be shown or handled inside.

```
Promise
    .then((info) => {
        return info
    })
    .catch((error) => {
        console.log(error)
    })
```

- using `try-catch` method

This try-catch method in promises will allow us to try some piece of code, if it satifies the condition it will return something orelse it will go to the catch block which will catch the error. BAsically try-catch block is useful to perform some actions/handle errors even before the promise is fullfiled or rejected.

```
try{

} catch(error) {
    console.log(error)
}
```

## Importance of error handling with promises

- As for async code it is generally very hard to debug the errors and hence using error handlers in promises will help us debug the code easily. THerefore proper error handling should be implemented to catch specific errors and fix them.
- Promise chains is a very good option for error handling. Because when a promise is rejected, the control will jump to the closest rejection handler.